var exports = module.exports = {};

//Hexadecimal Operations
exports.hex2raw = function (n) {
  var result = new Buffer(n, "hex");
  return result;
}

exports.convertToHex = function(str) {
    var hex = '';
    for(var i=0;i<str.length;i++) {
        hex += ''+str.charCodeAt(i).toString(16);
    }
    return hex;
}
