'use strict';

var getImplementation = function(type, platform){
  var imp;
  if(type === 'normal'){
    imp = require('./type/normal.js');
    imp.setPlatform(platform);
  }
  else{
    imp = require('./type/ipfs.js');
    imp.setPlatform(platform);
  }

  return imp;
}


module.exports = {
  getImplementation: getImplementation
};
