'use strict';

var appRoot = require('app-root-path');
var config = require(appRoot + '/configuration.js').config;
var erisDbImpl = require('erisdb_model');
var eris;
var plat = '';
var Log = require('log'),
  log = new Log(config.logLevel); // debug, info, error
var request = require('request');
var illyria = require('illyria');
var Rx = require('rx');
var ipfs = require('../ipfs.js');
var serverKeys = require('../providerServer.js');
var cryptoMod = require('../crypto.js');
var coding = require ('../coding.js');

var abi = null;
var address = null;
var addressEris;
var addressEth;
var contractName = 'smartPaymentIPFS';
var solidityServer = config.solidity.url;
var platformAccount;
var infoToSubscribe;
var clientIllyriaEris = illyria.createClient(config.erisFailOver.url, parseInt(config.erisFailOver.port));

var clientIllyriaEth = illyria.createClient(config.eth.failOver.url, parseInt(config.eth.failOver.port));

var setPlatform = function(platform){
  plat = platform;
  eris = erisDbImpl.getImplementation(platform);
  if(platform == 'eris'){
    platformAccount = config.eris.accountData;
  }
  else{
    platformAccount = config.eth.accountData;
  }
}

var logPattern = function (functionName){
  return '[smartPayment] f('+functionName+'): ';
};
var flagRetryingErisConnection = false;
var flagRetryingEthConnection = false;



function cipherDataPayment(payment, simetricKey){
  var cipheredData = {
    sourceAccount: payment.sourceAccount,
    sourceName: payment.sourceName,
    destinationAccount: payment.destinationAccount,
    destinationName: payment.destinationName,
    subject: payment.subject,
    amount: String(payment.amount)
  }
  var data = JSON.stringify(cipheredData);
  var ciphered = cryptoMod.cipher(data, simetricKey);
  return ciphered;

}

function decryptWithAllUUID(hashKeys, privKey){
  return new Promise ((resolve, reject) => {
    var key = '';
    return ipfs.get(hashKeys)
    .then(myKeys =>{
      var origin = myKeys.encriptedUUIDOrigin;
      var destiny = myKeys.encriptedUUIDDestiny;
      var auditor = myKeys.encriptedUUIDAuditor;

      try{
        log.info('Decrypt with encrypted UUID Source');
        key = cryptoMod.decryptKey(privKey, origin);
      }
      catch(err){
        try{
          log.info('Decrypt with encrypted UUID Bridge');
          key = cryptoMod.decryptKey(privKey, destiny);
        }
        catch(err){
          try{
            log.info('Decrypt with encrypted UUID Auditor');
            key = cryptoMod.decryptKey(privKey, auditor);
          }
          catch(err){
            log.info('All decrypts failed');
            key = '';
            reject(key);
          }
        }
      }
      resolve(key);
    })
    .catch(error =>{
      reject(key);
    })
  });
}

function decipherPayment(payment, privKey){
  return new Promise ((resolve, reject) => {
    //subscribe to the response of the future sending of 'sent' to contract
    var myKey = '';
    return decryptWithAllUUID(payment.keys,privKey)
    .then(key =>{
      myKey = key;
      return ipfs.get(payment.data);
    })
    .then(gotData =>{
      var decrypt = cryptoMod.decipher(gotData, myKey);
      var paymentdecrypt = JSON.parse(decrypt);
      paymentdecrypt.payId = payment.payId;
      paymentdecrypt.status = payment.status;
      paymentdecrypt.sourceAddress = payment.sourceAddress;
      paymentdecrypt.destinationBridgeAddress = payment.destinationBridgeAddress;
      paymentdecrypt.purchasing = payment.purchasing;
      paymentdecrypt.amount = parseFloat(paymentdecrypt.amount);
      paymentdecrypt.keys = payment.keys;
      resolve(paymentdecrypt);
    })
    .catch(error =>{
      log.error('Incorrect address, returning ciphered information');
      resolve(payment);
    })
  });
}

var eventSentStream = new Rx.Subject();
var eventAcceptedStream = new Rx.Subject();
var eventRetiredStream = new Rx.Subject();
var eventFailedStream = new Rx.Subject();
var eventSuccessStream = new Rx.Subject();
var eventTransactionErrorStream = new Rx.Subject();
var eventAll = eventSentStream
        .merge(eventRetiredStream)
        .merge(eventAcceptedStream)
        .merge(eventFailedStream)
        .merge(eventSuccessStream)
        .merge(eventTransactionErrorStream);

var logSubscription = eventAll.subscribe(data => log.debug(logPattern ('-logSubscription-') + `allEvents -> data: ${JSON.stringify(data)}`));


var loadContract = function (){
  log.info('loading contract');
  log.debug('getting contract');
  return new Promise( (resolve, reject ) => {
    request(
      {
        method: 'GET',
        url: solidityServer+'/contract/'+contractName+'/abi',
        timeout: 15*1000
      },
      function (error, response, body) {
        if(error){
          if (error.code === 'ETIMEDOUT'){
            log.error('retrying ');
            loadContract();
          } else if (error.code === 'ENOTFOUND' || error.code === 'ECONNREFUSED' || error.code === 'ECONNRESET') {
            log.error('not contract found, retrying ');
            setTimeout(loadContract, 15*1000);
          } else {
            log.error(error);
            reject(error);
          }
        }else{
          if(response.statusCode !== 200){
            log.error('retrying ');
            log.error('not contract found, retrying ');
            setTimeout(loadContract, 15*1000);
          }
          else{
            var jbody = JSON.parse(body);
            var item = {
              abi: jbody.abi,
              addressEris: jbody.addressEris,
              addressEth: jbody.addressEth
            }
            log.info(`got definition {${typeof abi}} for contract at ${jbody.addressEris}`);
            log.info(`got definition {${typeof abi}} for contract at ${jbody.addressEth}`);
            resolve(item);
          }
        }
      }
    );
  });
};

var genericEventHandler = function (evento, eventName, stream){
    var newEvent = {
      type: eventName,
      args: evento.args
    }
    stream.onNext(newEvent);
}

var eventSentHandler = (err, event) => { return genericEventHandler (event, 'eventSent',eventSentStream,  err);};
var eventAcceptedHandler = (err, event) => { return genericEventHandler (event, 'eventAccepted', eventAcceptedStream, err);};
var eventRetiredHandler = (err, event) => { return genericEventHandler (event, 'eventRetired',eventRetiredStream, err);};
var eventSuccessHandler = (err, event) => { return genericEventHandler (event, 'eventSuccess',eventSuccessStream, err);};
var eventFailedHandler = (err, event) => { return genericEventHandler (event, 'eventFailed',eventFailedStream, err);};
var eventTransactionErrorHandler = (err, event) => {
  log.error(logPattern('-event-')+'transaction error: ' + JSON.stringify(event));
  return genericEventHandler (event, 'eventTransactionError',eventTransactionErrorStream, err);
};

var subscribe = function (abi, address) {
  eris.eventWatcher(abi, address, 'eventSent', eventSentHandler );
  eris.eventWatcher(abi, address, 'eventAccepted', eventAcceptedHandler );
  eris.eventWatcher(abi, address, 'eventRetired', eventRetiredHandler );
  eris.eventWatcher(abi, address, 'eventFailed', eventFailedHandler );
  eris.eventWatcher(abi, address, 'eventTransactionError', eventTransactionErrorHandler );
}

var checkConnectionWithEris = function(){
  var intervalObject = setInterval(function(){
    var status = clientIllyriaEris.connectStatus();
    if(status != 'CONNECTED'){
      flagRetryingErisConnection = true;
      clientIllyriaEris.connect(function() {
        if(flagRetryingErisConnection) {
          flagRetryingErisConnection = false;
          var abi = infoToSubscribe.abi;
          var address = infoToSubscribe.addressEris;
          eris = erisDbImpl.getImplementation('eris');
          subscribe(abi, address);
        }
      });
    }
  }, config.erisFailOver.delayAttemps);
}

var checkConnectionWithEth = function(){
  var intervalObject = setInterval(function(){
    var status = clientIllyriaEth.connectStatus();
    if(status != 'CONNECTED'){
      flagRetryingEthConnection = true;
      clientIllyriaEth.connect(function() {
        if(flagRetryingEthConnection) {
          flagRetryingEthConnection = false;
          var abi = infoToSubscribe.abi;
          var address = infoToSubscribe.addressEth;
          eris = erisDbImpl.getImplementation('eth');
          subscribe(abi, address);
        }
      });
    }
  }, config.eth.failOver.delayAttemps);
}


var prepareFileToIPFS = function(name, payload){
  var files = [];
  var file = {
    name: name,
    payload: coding.convertToHex(JSON.stringify(payload))
  }
  files.push(file);
  return files;
}

var eventSend = function (data, name){
  //data preparation
  log.debug (`${logPattern('eventSend')} in, preparing data`);
  var platSourceAddress;
  var platContractAddress;
  if(plat == 'eris'){
    platSourceAddress = config.eris.accountData.address;
    platContractAddress = addressEris;
  }
  else{
    platSourceAddress = config.eth.accountData.address;
    platContractAddress = addressEth;
  }
  //1. map
  var sendData = {
      payId: data.payId, //bytes32
      sourceAddress: platSourceAddress, //address
      destinationBridgeAddress: data.destinationBridgeAddress, //address
      purchasing: '', //string
      keys: '', //string
      data: '', //string
      plat: plat
  };
  //2. encrypt
  var simetricKey = cryptoMod.generateUUID();
  var dataCiphered = cipherDataPayment(data, simetricKey);
  //3. encrypt keys
  var setKeysData = cryptoMod.cipherSimetricKeys(simetricKey);
  //4.send file and keys ciphered to ipfs
  log.debug (`${logPattern('eventSend')} storing files`);
  //TODO: cifrar archivos
  return ipfs.save(prepareFileToIPFS('data', dataCiphered))
  .then(dataIntoIpfs =>{
    log.debug (`${logPattern('eventSend')} ciphered data  stored ${JSON.stringify(dataIntoIpfs)}`);
    if (dataIntoIpfs && dataIntoIpfs.length) {
      sendData.data = dataIntoIpfs[0].Hash;
    }
    else {
      sendData.data = ''
    }
    return ipfs.save(prepareFileToIPFS('encryptedKeys', setKeysData));
  })
  .then(keyssent =>{
    log.debug (`${logPattern('eventSend')} encrypted keys stored ${JSON.stringify(keyssent)}`);
    if (keyssent && keyssent.length) {
      sendData.keys = keyssent[0].Hash;
    }
    else {
      sendData.keys = ''
    }
    for (var i = 0; i < data.attachments.length; i++){
      var cipherFile = cryptoMod.cipher(data.attachments[i].payload, simetricKey);
      data.attachments[i].payload = coding.convertToHex(cipherFile);
    }
    return ipfs.save(data.attachments);
  })
  //5. send 'send'
  .then(ipfsResponse => {
    log.debug (`${logPattern('eventSend')} stored ${JSON.stringify(ipfsResponse)}`);
    if (ipfsResponse && ipfsResponse.length) {
      sendData.purchasing = ipfsResponse.slice(-1)[0].Hash
    }
    else {
      sendData.purchasing = ''
    }
    log.debug (`${logPattern('eventSend')} purchasing field = ${sendData.purchasing}` );
    log.debug (`${logPattern('eventSend')} keys field = ${sendData.keys}` );
    log.debug (`${logPattern('eventSend')} data field = ${sendData.data}` );
    log.debug (`${logPattern('eventSend')} creating contract -send-` );
    //OJITO que hay que generalizar le invocacion del account data y el address ()
    var functionPromise = eris.callContractFunction(abi, platContractAddress, 'send' , sendData, platformAccount)
    return functionPromise;
  })
}



var eventBasic = function (data, abiFunctionName, reason){
  var processedData = {
    payId: data.payId, //bytes32
  };
  var platContractAddress;
  if(plat == 'eris'){
    platContractAddress = addressEris;
  }
  else{
    platContractAddress = addressEth;
  }
  if(reason){
    processedData.reason = reason;
  }
  log.debug (`${logPattern('eventBasic')} for(${abiFunctionName}) got ${JSON.stringify(data)} and sending ${JSON.stringify(processedData)}`);
  var functionPromise = eris.callContractFunction(abi, platContractAddress, abiFunctionName , processedData, platformAccount)
  return functionPromise;
}

//join an array of streams into one using 'merge' function
var joinStreams = function (streams){
  var targetStream = null;
  if (streams.length) {
    streams.forEach(stream => {
      if (targetStream){
        targetStream = targetStream.merge(stream);
      }
      else {
        targetStream = stream;
      }
    })
  }
  return targetStream;
};

var waitForEvent = function (payId, streams){
  //return promise with event
  return new Promise ((resolve, reject) => {
    log.info(`${logPattern('waitForEvent')} in - ${payId} / ${streams.length} streams`);

    //join streams, and if payID and sourceAddress then filter by both
    var targetStream = joinStreams(streams).first(event => {
      return (event.args.payId === payId &&  event.args.from === platformAccount.address);
    });
    if (targetStream == null) {
      reject('no streams defined')
    }

    //get event
    var subscription = targetStream.subscribe(event => {
      log.debug(`${logPattern('waitForEvent')} got event: ${JSON.stringify(event)}`);
      resolve(event);
    });
  })
}

//sends 'send' event and promises a result event
var eventSendSync = function(input){
  log.debug (`${logPattern('eventSendSync')} entering`);
  return eventSync(input.payId, input, [eventTransactionErrorStream, eventSentStream], eventSend);
}

//sends 'send' event and promises a result event
var eventRetiredSync = function(input){
  log.debug (`${logPattern('eventRetiredSync')} entering`);
  return eventSync(input.payId, input, [eventTransactionErrorStream, eventRetiredStream], x => eventBasic(x, 'retire'));
}

var eventAcceptSync = function (payId){
  var eventAccept = function (data){
    var processedData = data
    var platContractAddress;
    if(plat == 'eris'){
      platContractAddress = addressEris;
    }
    else{
      platContractAddress = addressEth;
    }
    log.debug (`${logPattern('eventAccept')} got ${JSON.stringify(data)} and sending ${JSON.stringify(processedData)}`);
    var functionPromise = eris.callContractFunction(abi, platContractAddress, 'accept' , processedData, platformAccount)
    return functionPromise;
  };

  log.debug (`${logPattern('eventAccept')} entering`);
  return eventSync(payId, {payId: payId}, [eventTransactionErrorStream, eventAcceptedStream], eventAccept);
}

var eventSuccessSync = function(payId){
  log.debug (`${logPattern('eventSuccessSync')} entering`);
  return eventSync(payId, {payId: payId}, [eventTransactionErrorStream, eventSuccessStream], x => eventBasic(x, 'success'));
}

var eventFailSync = function(payId, reason){
  log.debug (`${logPattern('eventFailSync')} entering`);
  return eventSync(payId, {payId: payId}, [eventTransactionErrorStream, eventFailedStream], x => eventBasic(x, 'fail', reason));
}

var eventSync = function(payId, input, streams, transactionCallFunction){
  log.debug (`${logPattern('eventSync')} entering`);
  return new Promise ((resolve, reject) => {
    //subscribe to the response of the future sending of 'sent' to contract
    var pEvent = waitForEvent(payId, streams);
    //send 'send' to contract
    var pSend = transactionCallFunction(input)
    Promise.all([pSend,pEvent])
    .then(x => {
      log.debug (`${logPattern('eventSync')} signal sent! ${JSON.stringify(x)}`);
      resolve (x[1]);
    })
    .catch( problem => {
      log.error(`${logPattern('eventSync')} problem with blockchain ${JSON.stringify(problem)}`);
      reject(`blockchain rejected: ${problem}`)
    })
  });
}


var getFunction = function (payId, name){
  var privKey = serverKeys.functions.getKeys(name).privKey;
  log.debug (`${logPattern('getFunction')} entering`);
  var solInput = {id: payId};
  var decipheredPayment = '';
  var hash = '';
  var platContractAddress;
  if(plat == 'eris'){
    platContractAddress = addressEris;
  }
  else{
    platContractAddress = addressEth;
  }
  var functionPromise = eris.callContractFunction(abi, platContractAddress, 'getterInfo', solInput, platformAccount)
  return functionPromise.then(payment =>{
    hash = payment.keys;
    return decipherPayment(payment, privKey)
  })
  .then(deciphered =>{
    decipheredPayment = deciphered;
    return ipfs.getFileLinks(decipheredPayment.purchasing);
  })
  .then(links =>{
    var myLinks = parseLsResponse(links);
    decipheredPayment.links = myLinks;
    return decipheredPayment;
  })
  .catch(error =>{
    return decipheredPayment;
  })
}

var parseLsResponse = function(lsResponse){
  var results = [];

  for( var i=0; i<lsResponse.length; i++){
     var result = {
       Name: '',
       url: ''
     }
     result.Name = lsResponse[i].Name;
     result.Hash = lsResponse[i].Hash;
     results.push(result);
  }
  return results;
}

var getLinks = function (hashFile){
  log.debug (`${logPattern('getLinks')} entering`);
  return ipfs.getFileLinks(hashFile);
}


function decipherFiles(hash, hashkeys, owner){
  var privKey = serverKeys.functions.getKeys(owner).privKey;
  return new Promise ((resolve, reject) => {
    //subscribe to the response of the future sending of 'sent' to contract
    var myKey = '';
    return decryptWithAllUUID(hashkeys, privKey)
    .then(key =>{
      myKey = key;
      return ipfs.get(hash);
    })
    .then(gotData =>{
      var decrypt = cryptoMod.decipher(gotData.data, myKey);
      decrypt = coding.hex2raw(decrypt);
      resolve(decrypt);
    })
    .catch(error =>{
      log.error('Incorrect address, returning ciphered information');
      reject(error);
    })
  });
}

var pin = function (hash){
  return ipfs.pin(hash);
}

var init = function() {
  loadContract()
  .then(loaded =>{
     infoToSubscribe = loaded;
     abi = infoToSubscribe.abi;
     for(var plat in config.platforms){
       if(config.platforms[plat] === 'eris'){
         addressEris = infoToSubscribe.addressEris;
         checkConnectionWithEris();
       }
       else {
         addressEth = infoToSubscribe.addressEth;
         checkConnectionWithEth();
       }
     }
  })
}

init();


module.exports = {
  streams: {
    sentStream: eventSentStream,
    acceptedStream: eventAcceptedStream,
    retiredStream: eventRetiredStream,
    failedStream: eventFailedStream,
    successStream: eventSuccessStream,
    transactionErrorStream: eventTransactionErrorStream,
    all: eventAll
  },
  functions:{
    send: eventSendSync,
    get: getFunction,
    retire: eventRetiredSync,
    accept: eventAcceptSync,
    success: eventSuccessSync,
    doFail: eventFailSync,
    pin: pin,
    getLinks: getLinks,
    decipherFiles: decipherFiles
  },
  keys: {},
  setPlatform: setPlatform,
  waitForEvent: waitForEvent
};
