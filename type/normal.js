'use strict';

var appRoot = require('app-root-path');
var config = require(appRoot + '/configuration.js').config;
var erisDbImpl = require('erisdb_model');
var eris;
var plat = '';
var Log = require('log'),
  log = new Log(config.logLevel); // debug, info, error
var request = require('request');
var illyria = require('illyria');
var Rx = require('rx');
var serverKeys = require('../providerServer.js');
var cryptoMod = require('../crypto.js');
var coding = require ('../coding.js');

var abi = null;
var address = null;
var addressEris;
var addressEth;
var contractName = 'smartPaymentNormal';
var solidityServer = config.solidity.url;
var platformAccount;
var infoToSubscribe;
var clientIllyriaEris = illyria.createClient(config.erisFailOver.url, parseInt(config.erisFailOver.port));

var clientIllyriaEth = illyria.createClient(config.eth.failOver.url, parseInt(config.eth.failOver.port));

var setPlatform = function(platform){
  plat = platform;
  eris = erisDbImpl.getImplementation(platform);
  if(platform == 'eris'){
    platformAccount = config.eris.accountData;
  }
  else{
    platformAccount = config.eth.accountData;
  }
}

var logPattern = function (functionName){
  return '[smartPayment] f('+functionName+'): ';
};
var flagRetryingErisConnection = false;
var flagRetryingEthConnection = false;



function cipherDataPayment(payment, simetricKey){
  var cipheredSimetricKeys = cryptoMod.cipherSimetricKeys(simetricKey);
  var cipheredData = {
    payId: payment.payId,
    sourceAccount: cryptoMod.cipher(payment.sourceAccount, simetricKey),
    sourceName: cryptoMod.cipher(payment.sourceName, simetricKey),
    destinationAccount: cryptoMod.cipher(payment.destinationAccount, simetricKey),
    destinationName: cryptoMod.cipher(payment.destinationName, simetricKey),
    destinationBridgeAddress: payment.destinationBridgeAddress,
    subject: cryptoMod.cipher(payment.subject, simetricKey),
    amount: cryptoMod.cipher(String(payment.amount), simetricKey),
    purchasing: cryptoMod.cipher(payment.purchasing, simetricKey),
    sessionKeyOrigin: serverKeys.functions.getKeys('source').pubKey,
    sessionKeyDestiny: serverKeys.functions.getKeys('bridge').pubKey,
    sessionKeyAuditor: serverKeys.functions.getKeys('auditor').pubKey,
    encriptedUUIDOrigin: cipheredSimetricKeys.encriptedUUIDOrigin,
    encriptedUUIDDestiny: cipheredSimetricKeys.encriptedUUIDDestiny,
    encriptedUUIDAuditor: cipheredSimetricKeys.encriptedUUIDAuditor,
    plat: plat
  }
  return cipheredData;
}

function decryptWithAllUUID(payment, privKey){
  var key = '';
  try{
    log.info('Decrypt with encrypted UUID Source');
    key = cryptoMod.decryptKey(privKey, payment.encriptedUUIDOrigin);
  }
  catch(err){
    try{
      log.info('Decrypt with encrypted UUID Bridge');
      key = cryptoMod.decryptKey(privKey, payment.encriptedUUIDDestiny);
    }
    catch(err){
      try{
        log.info('Decrypt with encrypted UUID Auditor');
        key = cryptoMod.decryptKey(privKey, payment.encriptedUUIDAuditor);
      }
      catch(err){
        log.info('All decrypts failed');
        key = '';
      }
    }
  }
  return key;
}

function decipherPayment(payment, privKey){
  return new Promise ((resolve, reject) => {
    //subscribe to the response of the future sending of 'sent' to contract
    try {
      var key = decryptWithAllUUID(payment, privKey);
      var payload = {
        payId: payment.payId,
        status: payment.status,
        sourceAccount: cryptoMod.decipher(payment.sourceAccount, key),
        sourceAddress: payment.sourceAddress,
        sourceName: cryptoMod.decipher(payment.sourceName, key),
        destinationAccount: cryptoMod.decipher(payment.destinationAccount, key),
        destinationName: cryptoMod.decipher(payment.destinationName, key),
        destinationBridgeAddress: payment.destinationBridgeAddress,
        amount: parseFloat(cryptoMod.decipher(payment.amount, key)),
        subject: cryptoMod.decipher(payment.subject, key),
        purchasing:  cryptoMod.decipher(payment.purchasing, key)
      }
      resolve(payload);
    }
    catch(err){
      log.error('Incorrect address, returning ciphered information');
      resolve(payment);
    }
  });
}

var eventSentStream = new Rx.Subject();
var eventAcceptedStream = new Rx.Subject();
var eventRetiredStream = new Rx.Subject();
var eventFailedStream = new Rx.Subject();
var eventSuccessStream = new Rx.Subject();
var eventTransactionErrorStream = new Rx.Subject();
var eventAll = eventSentStream
        .merge(eventRetiredStream)
        .merge(eventAcceptedStream)
        .merge(eventFailedStream)
        .merge(eventSuccessStream)
        .merge(eventTransactionErrorStream);

var logSubscription = eventAll.subscribe(data => log.debug(logPattern ('-logSubscription-') + `allEvents -> data: ${JSON.stringify(data)}`));


var loadContract = function (){
  log.info('loading contract');
  log.debug('getting contract');
  return new Promise( (resolve, reject ) => {
    request(
      {
        method: 'GET',
        url: solidityServer+'/contract/'+contractName+'/abi',
        timeout: 15*1000
      },
      function (error, response, body) {
        if(error){
          if (error.code === 'ETIMEDOUT'){
            log.error('retrying ');
            loadContract();
          } else if (error.code === 'ENOTFOUND' || error.code === 'ECONNREFUSED' || error.code === 'ECONNRESET') {
            log.error('not contract found, retrying ');
            setTimeout(loadContract, 15*1000);
          } else {
            log.error(error);
            reject(error);
          }
        }else{
          if(response.statusCode !== 200){
            log.error('retrying ');
            log.error('not contract found, retrying ');
            setTimeout(loadContract, 15*1000);
          }
          else{
            var jbody = JSON.parse(body);
            var item = {
              abi: jbody.abi,
              addressEris: jbody.addressEris,
              addressEth: jbody.addressEth
            }
            log.info(`got definition {${typeof abi}} for contract at ${jbody.addressEris}`);
            log.info(`got definition {${typeof abi}} for contract at ${jbody.addressEth}`);
            resolve(item);
          }
        }
      }
    );
  });
};

var genericEventHandler = function (evento, eventName, stream){
    var newEvent = {
      type: eventName,
      args: evento.args
    }
    stream.onNext(newEvent);
}

var eventSentHandler = (err, event) => { return genericEventHandler (event, 'eventSent',eventSentStream,  err);};
var eventAcceptedHandler = (err, event) => { return genericEventHandler (event, 'eventAccepted', eventAcceptedStream, err);};
var eventRetiredHandler = (err, event) => { return genericEventHandler (event, 'eventRetired',eventRetiredStream, err);};
var eventSuccessHandler = (err, event) => { return genericEventHandler (event, 'eventSuccess',eventSuccessStream, err);};
var eventFailedHandler = (err, event) => { return genericEventHandler (event, 'eventFailed',eventFailedStream, err);};
var eventTransactionErrorHandler = (err, event) => {
  log.error(logPattern('-event-')+'transaction error: ' + JSON.stringify(event));
  return genericEventHandler (event, 'eventTransactionError',eventTransactionErrorStream, err);
};

var subscribe = function (abi, address) {
  eris.eventWatcher(abi, address, 'eventSent', eventSentHandler );
  eris.eventWatcher(abi, address, 'eventAccepted', eventAcceptedHandler );
  eris.eventWatcher(abi, address, 'eventRetired', eventRetiredHandler );
  eris.eventWatcher(abi, address, 'eventFailed', eventFailedHandler );
  eris.eventWatcher(abi, address, 'eventTransactionError', eventTransactionErrorHandler );
}

var checkConnectionWithEris = function(){
  var intervalObject = setInterval(function(){
    var status = clientIllyriaEris.connectStatus();
    if(status != 'CONNECTED'){
      flagRetryingErisConnection = true;
      clientIllyriaEris.connect(function() {
        if(flagRetryingErisConnection) {
          flagRetryingErisConnection = false;
          var abi = infoToSubscribe.abi;
          var address = infoToSubscribe.addressEris;
          eris = erisDbImpl.getImplementation('eris');
          subscribe(abi, address);
        }
      });
    }
  }, config.erisFailOver.delayAttemps);
}

var checkConnectionWithEth = function(){
  var intervalObject = setInterval(function(){
    var status = clientIllyriaEth.connectStatus();
    if(status != 'CONNECTED'){
      flagRetryingEthConnection = true;
      clientIllyriaEth.connect(function() {
        if(flagRetryingEthConnection) {
          flagRetryingEthConnection = false;
          var abi = infoToSubscribe.abi;
          var address = infoToSubscribe.addressEth;
          eris = erisDbImpl.getImplementation('eth');
          subscribe(abi, address);
        }
      });
    }
  }, config.eth.failOver.delayAttemps);
}

var eventSend = function (data, name){
  //data preparation
  log.debug (`${logPattern('eventSend')} in, preparing data`);
  var platSourceAddress;
  var platContractAddress;
  if(plat == 'eris'){
    platSourceAddress = config.eris.accountData.address;
    platContractAddress = addressEris;
  }
  else{
    platSourceAddress = config.eth.accountData.address;
    platContractAddress = addressEth;
  }
  //1. map
  var sendData = {
    payId: data.payId, //bytes32
    sourceAccount: data.sourceAccount, //bytes32
    sourceName: data.sourceName, //bytes32
    sourceAddress: platSourceAddress, //address
    destinationAccount: data.destinationAccount, //bytes32
    destinationName: data.destinationName, //bytes32
    destinationBridgeAddress: data.destinationBridgeAddress, //address
    subject: data.subject, //bytes32
    amount: String(data.amount) , //bytes32
    purchasing: data.purchasing, //bytes32
    plat: plat
  }
  //2. encrypt
  var simetricKey = cryptoMod.generateUUID();
  var dataCiphered = cipherDataPayment(data, simetricKey);
  dataCiphered.sourceAddress = platSourceAddress;
  //3. send ciphered payment
  return eris.callContractFunction(abi,platContractAddress, 'send' , dataCiphered, platformAccount)
  .then(sent => {
    var keys = {
      payId: data.payId,
      sessionKeyOrigin: dataCiphered.sessionKeyOrigin,
      sessionKeyDestiny: dataCiphered.sessionKeyDestiny,
      sessionKeyAuditor: dataCiphered.sessionKeyAuditor,
      encriptedUUIDOrigin: dataCiphered.encriptedUUIDOrigin,
      encriptedUUIDDestiny: dataCiphered.encriptedUUIDDestiny,
      encriptedUUIDAuditor: dataCiphered.encriptedUUIDAuditor
    }
    var functionPromise = eris.callContractFunction(abi,platContractAddress, 'setKeys' , keys, platformAccount)
    return functionPromise;
  })
}



var eventBasic = function (data, abiFunctionName, reason){
  var processedData = {
    payId: data.payId, //bytes32
  };
  var platContractAddress;
  if(plat == 'eris'){
    platContractAddress = addressEris;
  }
  else{
    platContractAddress = addressEth;
  }
  if(reason){
    processedData.reason = reason;
  }
  log.debug (`${logPattern('eventBasic')} for(${abiFunctionName}) got ${JSON.stringify(data)} and sending ${JSON.stringify(processedData)}`);
  var functionPromise = eris.callContractFunction(abi, platContractAddress, abiFunctionName , processedData, platformAccount)
  return functionPromise;
}

//join an array of streams into one using 'merge' function
var joinStreams = function (streams){
  var targetStream = null;
  if (streams.length) {
    streams.forEach(stream => {
      if (targetStream){
        targetStream = targetStream.merge(stream);
      }
      else {
        targetStream = stream;
      }
    })
  }
  return targetStream;
};

var waitForEvent = function (payId, streams){
  //return promise with event
  return new Promise ((resolve, reject) => {
    log.info(`${logPattern('waitForEvent')} in - ${payId} / ${streams.length} streams`);

    //join streams, and if payID and sourceAddress then filter by both
    var targetStream = joinStreams(streams).first(event => {
      return (event.args.payId === payId &&  event.args.from === platformAccount.address);
    });
    if (targetStream == null) {
      reject('no streams defined')
    }

    //get event
    var subscription = targetStream.subscribe(event => {
      log.debug(`${logPattern('waitForEvent')} got event: ${JSON.stringify(event)}`);
      resolve(event);
    });
  })
}

//sends 'send' event and promises a result event
var eventSendSync = function(input){
  log.debug (`${logPattern('eventSendSync')} entering`);
  return eventSync(input.payId, input, [eventTransactionErrorStream, eventSentStream], eventSend);
}

//sends 'send' event and promises a result event
var eventRetiredSync = function(input){
  log.debug (`${logPattern('eventRetiredSync')} entering`);
  return eventSync(input.payId, input, [eventTransactionErrorStream, eventRetiredStream], x => eventBasic(x, 'retire'));
}

var eventAcceptSync = function (payId){
  var eventAccept = function (data){
    var processedData = data
    var platContractAddress;
    if(plat == 'eris'){
      platContractAddress = addressEris;
    }
    else{
      platContractAddress = addressEth;
    }
    log.debug (`${logPattern('eventAccept')} got ${JSON.stringify(data)} and sending ${JSON.stringify(processedData)}`);
    var functionPromise = eris.callContractFunction(abi, platContractAddress, 'accept' , processedData, platformAccount)
    return functionPromise;
  };

  log.debug (`${logPattern('eventAccept')} entering`);
  return eventSync(payId, {payId: payId}, [eventTransactionErrorStream, eventAcceptedStream], eventAccept);
}

var eventSuccessSync = function(payId){
  log.debug (`${logPattern('eventSuccessSync')} entering`);
  return eventSync(payId, {payId: payId}, [eventTransactionErrorStream, eventSuccessStream], x => eventBasic(x, 'success'));
}

var eventFailSync = function(payId, reason){
  log.debug (`${logPattern('eventFailSync')} entering`);
  return eventSync(payId, {payId: payId}, [eventTransactionErrorStream, eventFailedStream], x => eventBasic(x, 'fail', reason));
}

var eventSync = function(payId, input, streams, transactionCallFunction){
  log.debug (`${logPattern('eventSync')} entering`);
  return new Promise ((resolve, reject) => {
    //subscribe to the response of the future sending of 'sent' to contract
    var pEvent = waitForEvent(payId, streams);
    //send 'send' to contract
    var pSend = transactionCallFunction(input)
    Promise.all([pSend,pEvent])
    .then(x => {
      log.debug (`${logPattern('eventSync')} signal sent! ${JSON.stringify(x)}`);
      resolve (x[1]);
    })
    .catch( problem => {
      log.error(`${logPattern('eventSync')} problem with blockchain ${JSON.stringify(problem)}`);
      reject(`blockchain rejected: ${problem}`)
    })
  });
}

var callContractGetter = function (abi, myaddress, params, myaccount) {
  var functionName = 'getterData';
  var result = {};

  return new Promise((resolve, reject) => {
    eris.callContractFunction(abi, myaddress, functionName, params, myaccount)
    .then( dataGot => {
      result = dataGot;
      // two calls are needed to collect all fields
      return eris.callContractFunction(abi, myaddress, 'getterInfo', params, myaccount)
    })
    .then( infoGot => {
      result.payId = infoGot.payId;
      result.status = infoGot.status;
      result.sourceAddress = infoGot.sourceAddress;
      result.destinationBridgeAddress = infoGot.destinationBridgeAddress;
      result.parentId = infoGot.parentId;
      return eris.callContractFunction(abi, myaddress, 'getterKeys', params, myaccount)
      .then(keysGot =>{
        result.publicKeyOrigin = keysGot.sessionKeyOrigin;
        result.publicKeyDestiny = keysGot.sessionKeyDestiny;
        result.publicKeyAuditor = keysGot.sessionKeyAuditor;
        result.encriptedUUIDOrigin = keysGot.encriptedUUIDOrigin;
        result.encriptedUUIDDestiny = keysGot.encriptedUUIDDestiny;
        result.encriptedUUIDAuditor = keysGot.encriptedUUIDAuditor;
        resolve(result);
      })
    })
    .catch(reject);
  });
}

var getFunction = function (payId, name){
  var platContractAddress;
  if(plat == 'eris'){
    platContractAddress = addressEris;
  }
  else{
    platContractAddress = addressEth;
  }
  var privKey = serverKeys.functions.getKeys(name).privKey;
  log.debug (`${logPattern('getFunction')} entering`);
  var solInput = {id: payId};
  var functionPromise = callContractGetter(abi, platContractAddress, solInput, platformAccount)
  return functionPromise.then(payment =>{
    return decipherPayment(payment, privKey);
  })
}

var parseLsResponse = function(lsResponse){
  var results = [];

  for( var i=0; i<lsResponse.length; i++){
     var result = {
       Name: '',
       url: ''
     }
     result.Name = lsResponse[i].Name;
     result.Hash = lsResponse[i].Hash;
     results.push(result);
  }
  return results;
}

var init = function() {
  loadContract()
  .then(loaded =>{
     infoToSubscribe = loaded;
     abi = infoToSubscribe.abi;
     for(var plat in config.platforms){
       if(config.platforms[plat] === 'eris'){
         addressEris = infoToSubscribe.addressEris;
         checkConnectionWithEris();
       }
       else {
         addressEth = infoToSubscribe.addressEth;
         checkConnectionWithEth();
       }
     }
  })
}

init();


module.exports = {
  streams: {
    sentStream: eventSentStream,
    acceptedStream: eventAcceptedStream,
    retiredStream: eventRetiredStream,
    failedStream: eventFailedStream,
    successStream: eventSuccessStream,
    transactionErrorStream: eventTransactionErrorStream,
    all: eventAll
  },
  functions:{
    send: eventSendSync,
    get: getFunction,
    retire: eventRetiredSync,
    accept: eventAcceptSync,
    success: eventSuccessSync,
    doFail: eventFailSync
  },
  keys: {},
  setPlatform: setPlatform,
  waitForEvent: waitForEvent
};
