module.exports = {
  logLevel: 'debug', // debug, info, error
  eris: {
    chain: {
      name: 'serenity'
    },
    node0: {
      url: 'http://192.168.99.100:1337/rpc',
      wsrul: 'ws://192.168.99.100:1337/socketrpc',
      dbUrl: 'http://192.168.99.100:46657/'
    },
    compilers: {
      url: 'http://192.168.99.100:9091/compile'
        // url:'https://compilers.eris.industries:8091/compile'
    },
    keys: {
      url: 'http://192.168.99.100:4767/sign'
    },
    node1: {},
    accountData: {
      'address': '3113C254CADC6DCEFDAA0E578FB86ABF69C9FDD5', //participant 4
      'pubKey': 'D0F85812DB8035AA87D80C66AB36DFD52B7F85710D865FA3B23CE6FD6ECEC7E0',
      'privKey': 'FA8C654D3A583FA83361F466D1BF6B5992277D0CCB7A16B84454254F67C90386D0F85812DB8035AA87D80C66AB36DFD52B7F85710D865FA3B23CE6FD6ECEC7E0'
    },
    auditorData: {
      'address': '2F94F5108C11F183AA55D396E1E2B414DF0DBC0A',
      'pubKey':   'LS0tLS1CRUdJTiBQVUJMSUMgS0VZLS0tLS0KTUlJQklqQU5CZ2txaGtpRzl3MEJB\n'
                 +'UUVGQUFPQ0FROEFNSUlCQ2dLQ0FRRUEzczNFdTJQdFRERlhMUnQ0NzVrUgpJNHFr\n'
                 +'MXlXWVIzUXl6M1gxM2ZpZi95U2ZoQUtDeWJSa0FNVitDTENWZy9ZQVY4SHVPMHg2\n'
                 +'aUZEYlUvZ0o2RW1zCnkzQ2tGVGVJT0JqeEFtMWVsb2JlbjMxS0t0a0VSOVBROU9U\n'
                 +'ZnJSek83dEhKVmx1OGoxOVRRZ1dEQzVOTmlrSjcKK1JBZTdGN0xVRmpCY2lGdWE2\n'
                 +'eUxqZkxlZkFMRXppMTR0R29aQVVPWG00RFZaaUdiZTVyZTFjSHR3NjNLVHlzagoy\n'
                 +'S1BLZVU3d0M3dTBZNnd4TmRtcUVNbmFUYnVBeklXSE1EMnJaM1hQNGFCdWMvSGdk\n'
                 +'OXVKeUJtalpSMkYwVkRCClFscjZ2Sk5UTU5TK0hFZ1dnZ09ZK1JjNVRPa3l0Q1FV\n'
                 +'WHQzNy9iclRjUDYxQWN0bFNlbzYxZlMwd2Y0SnRLUEwKQXdJREFRQUIKLS0tLS1F\n'
                 +'TkQgUFVCTElDIEtFWS0tLS0tCg==',
      'privKey':  'LS0tLS1CRUdJTiBSU0EgUFJJVkFURSBLRVktLS0tLQpNSUlFcFFJQkFBS0NBUUVB\n'
                 +'M3MzRXUyUHRUREZYTFJ0NDc1a1JJNHFrMXlXWVIzUXl6M1gxM2ZpZi95U2ZoQUtD\n'
                 +'CnliUmtBTVYrQ0xDVmcvWUFWOEh1TzB4NmlGRGJVL2dKNkVtc3kzQ2tGVGVJT0Jq\n'
                 +'eEFtMWVsb2JlbjMxS0t0a0UKUjlQUTlPVGZyUnpPN3RISlZsdThqMTlUUWdXREM1\n'
                 +'Tk5pa0o3K1JBZTdGN0xVRmpCY2lGdWE2eUxqZkxlZkFMRQp6aTE0dEdvWkFVT1ht\n'
                 +'NERWWmlHYmU1cmUxY0h0dzYzS1R5c2oyS1BLZVU3d0M3dTBZNnd4TmRtcUVNbmFU\n'
                 +'YnVBCnpJV0hNRDJyWjNYUDRhQnVjL0hnZDl1SnlCbWpaUjJGMFZEQlFscjZ2Sk5U\n'
                 +'TU5TK0hFZ1dnZ09ZK1JjNVRPa3kKdENRVVh0MzcvYnJUY1A2MUFjdGxTZW82MWZT\n'
                 +'MHdmNEp0S1BMQXdJREFRQUJBb0lCQVFDZHEyd3ZXRFhUbDQ4agpvcTlnanllcnlT\n'
                 +'K0IrVjdjSWdUYS81VXdzZ00zb1lrbytFWHB5N2lCTzBGSGtQOGQxWGJFbzc2ZFJD\n'
                 +'L3FTNTRLCnZCcjJ3VHlBODRPS2FEUUR6dEt2YndwaTU2cWZueUJmVmhoTFpFQVpM\n'
                 +'ZWFFTVVEWGpLbzRiTTd4ZnZvQ1hMZ3gKOXRvR242bnZ4Vks4M3hCdUdZRTc3U0No\n'
                 +'SGdmY09OTFB4WFk1ejVCWWU0L3dvSi93WUFETVh5ZUhvNU5EL0JDdQpOc2FtdmQv\n'
                 +'anBJeWJuYzdZSUxvc3VGdzY4V0RHWXg2alZ4ZnZydEc3eWQ2N3laVjQxVDluNUs5\n'
                 +'dnpOOXZtTU9CClo1TkN5cmphU2FHTEF6SXRTSzh0dHdHbGVoUXdTNGFvaCtSR0p5\n'
                 +'dk1hWGVTNXVxT3ZmaTFIOFNRRGtTdEFXSFkKdEVmSDhaZkJBb0dCQVBTTTYwZDE1\n'
                 +'QkRLZGV2MTVoQ2F3TTNsY1NIS1lFN08yNXFVc1h2dDlNNkU0N0lLQUNNaQo2eW1P\n'
                 +'UGMwVDc3blMrQmozMVNQNDdxbFV5bXFHM083RENVSTg0U0FVM1dlT3RpeU04dHc1\n'
                 +'cGpLYnhhclpjclJ1CnJrVElGalJxNGRneUJKNE5ESWYycWM4dEZuMjg2N051T3kz\n'
                 +'MG84SlByTnBUVk1teC9hS1FsSmV4QW9HQkFPazgKTStKQWZIeGV4cys1Z0xKZldZ\n'
                 +'cUg4VldRNHlmNjVHWjlqdWlwbGZIZmZScmxUeDQzb1NoK3RoYS9kcnJCWG9SWgpn\n'
                 +'QktVZytkVGNXM1NhTTQweHNDbzdiY1QyT2FqNWd3RjdtMFU3RWRVSFNnZVhNcUVa\n'
                 +'b0J5Z2tjbEw5cEVLbENDCm9JMGV6b3c1N2o0ZkZIMlYxODkvdnBXY0FBbEtHZHUw\n'
                 +'cmlwVk5pN3pBb0dCQUxxNFNnQ0RCQUxLQXlXSjJBSy8KbGd6ZmJmVGw1NGprcW9v\n'
                 +'cGxDWlN0c05rZGRIL2pzRkZMUmJLazJkZEhJSGVraTNHTk1oRmpqeGVFRHNzQTg2\n'
                 +'Tgp2aWtFQU1RVkNrUENsRmdKdW5qcHozcitzdnRURWlDd0RZRlk0Sk5mTkZkMVFV\n'
                 +'OUlHVlJLd3JiNzd4dE1QdzcrClVGTXpZa0cvd2ZQbEFtMEo2T1hSekpMUkFvR0JB\n'
                 +'SURWVVcrVDRXK3R4ckl0ck1wYTR4SWpuNHA5V3d6VmlONlkKVXAxd043U0FtQkdJ\n'
                 +'UXhqVTc3OTNwc3VQL1Z5UnZjenJ4MVA4RjVSclFrZkFwMFhvWHpWQzJ2UjdFUi90\n'
                 +'dndBNwpNb1dzdmd0MGlocUVFd3VHOFR2WWhPQVp5WUlPanpVand0M1ErNVQzMXdN\n'
                 +'WGxmRDlHVlcwSWFsd241Q0NEeUNPCmdIby8wWXREQW9HQUpHZ1haS2dmYUREaFJK\n'
                 +'bWdVY1B2NzdBQVVJa2xLZFVZeFFSYVhPOXo2VDI4SG81Q1EwajEKUTVQT2xTNTRS\n'
                 +'LzNtekxPTnJEQnE1SmZpUklzUlU3cDNlOHh6NGdkNDNndzFVYjY5OUF0Y3JjY25V\n'
                 +'VUhpS1QvMApFM3RoM3ZVR1JZYXFLcEZsdGQzTWxMR29yL3BhYnZ5WVdIZEp6WkE0\n'
                 +'MldVOTVPWVpDTG1mekt3PQotLS0tLUVORCBSU0EgUFJJVkFURSBLRVktLS0tLQo='
    },
    sourceData: {
      'address': '3DDD7572D4F8D65109CFBF8AB5294C74A4C904AA',
      'pubKey':    'LS0tLS1CRUdJTiBQVUJMSUMgS0VZLS0tLS0KTUlJQklqQU5CZ2txaGtpRzl3MEJB\n'
                  +'UUVGQUFPQ0FROEFNSUlCQ2dLQ0FRRUFzbGxMV2crdVA0eGd4bTVQQldvMQpUTFVY\n'
                  +'bHkrdmpkUXl2emtTRFVYcVlua2ZxV0tMcW5Xa0RPUThuZU1uZnhPeFp1bjNGdzZk\n'
                  +'cU9yd1JnVHROWTZ1CkZvQXQ5RTByOGZtSkF3aGZsV2xZYlZYaE5GdFEyZ1phSmw2\n'
                  +'R2NqdW5uaFZGdURjbTFvc21qVmkxbHFRVkVONzkKcDlEVVBrVGZPT3RpdDRQNWNp\n'
                  +'dmNrWnp5Rmo2SEZPdDJYdHQ5QXJUSFdFa3lDZHdJZFdvOE45bEtUT1crZ1lXOAp6\n'
                  +'dVdPTTZIZmdkbnBQUUF1R1JobnN2cm52MzBScXBtK05adC8ycHl4SjVBRWw4c21t\n'
                  +'d1JIWFZaNFlFZ2R2SnU2Cnk1UHFOSHVaZ2hXUUt0TUdJcjMyaDdwM2pQSUwreXRG\n'
                  +'SDNtL2lTUzlYLzh0QTl2eC9KRnA2N3plSFNwYzdURisKZXdJREFRQUIKLS0tLS1F\n'
                  +'TkQgUFVCTElDIEtFWS0tLS0tCg==',
      'privKey':   'LS0tLS1CRUdJTiBSU0EgUFJJVkFURSBLRVktLS0tLQpNSUlFb1FJQkFBS0NBUUVB\n'
                  +'c2xsTFdnK3VQNHhneG01UEJXbzFUTFVYbHkrdmpkUXl2emtTRFVYcVlua2ZxV0tM\n'
                  +'CnFuV2tET1E4bmVNbmZ4T3hadW4zRnc2ZHFPcndSZ1R0Tlk2dUZvQXQ5RTByOGZt\n'
                  +'SkF3aGZsV2xZYlZYaE5GdFEKMmdaYUpsNkdjanVubmhWRnVEY20xb3NtalZpMWxx\n'
                  +'UVZFTjc5cDlEVVBrVGZPT3RpdDRQNWNpdmNrWnp5Rmo2SApGT3QyWHR0OUFyVEhX\n'
                  +'RWt5Q2R3SWRXbzhOOWxLVE9XK2dZVzh6dVdPTTZIZmdkbnBQUUF1R1JobnN2cm52\n'
                  +'MzBSCnFwbStOWnQvMnB5eEo1QUVsOHNtbXdSSFhWWjRZRWdkdkp1Nnk1UHFOSHVa\n'
                  +'Z2hXUUt0TUdJcjMyaDdwM2pQSUwKK3l0RkgzbS9pU1M5WC84dEE5dngvSkZwNjd6\n'
                  +'ZUhTcGM3VEYrZXdJREFRQUJBb0lCQUNSRWNrVkRNUXp0TC9USwpFN21uS21XSjRk\n'
                  +'MDFyajVxSzFPbGVGMUluV3dlODJoUGlOVkdEUEV1TElGeTR1STlqL0s4bXltT0tG\n'
                  +'TmtTeTRCCjdIYmFwOTRkZkxyVVFwNTNQdnhsNUlJT3BSTyt4Z2dPbGJkd1dUNjZO\n'
                  +'Qjdnc0tvOTZ5cVYxSWtHVFBXclYybmwKdmY3clNhdXBsSytBODkrd1JpZkpaNTFt\n'
                  +'Ky9pRUdlcE5XYmlOSHlDcURMbHNNUkRMOWVZWjNLOGpRN3JqWXBGZgpmZGxUUWM1\n'
                  +'cXJyM2YxWTVaSTJsZ1l3SnI0Wk9XSUdxem9lY2hhWlBCaGM1M204d1dHWTRSQmQ3\n'
                  +'ZHJFSWRQM1ZQCjZrdGphZjMyUXA1L0ZRbzlDUHdJeFAzZTUvK1ZhRDFibkxLNW45\n'
                  +'SXRuUlhvSXo3RCtyWWd6LzlOSzQ1Nmt4SjIKRlZxR0Jya0NnWUVBMjlaQTRWVXYr\n'
                  +'S3hmeC9WaDNXd0xmWjdxMlNVMHlFa2RYQitTUEJIc2E4d1NCaWlISDJ0RgptWnpK\n'
                  +'dDFNbWdydmxML0xLNmNqbG52MDVvZ1puaytzRXJOMTA2WjJDcFFxd01vTmlsYk1Y\n'
                  +'alVKM3MvRHlVa29KCnhaY21RR2ErOXlLdEhjMm10K1BBM2pBV3lRbnNYZml5Wjla\n'
                  +'bEMxc29vYWcxZ20rMjNINjJZdzBDZ1lFQXo2L2wKNTQreGdpaG5sbnN6b2NZSW1P\n'
                  +'MmxSa1NxUkI0OGJqKzZOMmlheis2VzlrWWFscUdqMnhmb253VWdOU2QvWGpzUwpF\n'
                  +'eFV0a1ZHM212Rmx1amdsdTdvMTVDdGVDWVN5NGQ1ZVdGcXpIaDlseFpHTTNDa0sw\n'
                  +'NXFUWUovYlQwSEIxOGhtCitDWnVWRmd1NXEySGRDYTI2U0tRRk9jeFhkOEdLb28z\n'
                  +'K2VYK0phY0NnWUEvYmFFSW9zaVZpajZVZGdOSG5LM3cKRDlNWTFmaFZ0V3JJTXIv\n'
                  +'Qm56WkVuL3V2VlRBdmE0Y0lYRW1ESUhXakVNMSt1U0c4dEY4ME1VSFlzdHBkdmxo\n'
                  +'NQorK2VJcEE2V3llK3VrMlAxWjFmclJQcytxczF2MnJiWkVOUlRqcEdZd3FPS1R0\n'
                  +'VEVoKzBVN25FVWprMXBpNkVGCi9zRG10dVdSUnZReGIwUW1lRlR0WFFKL2FrYVlY\n'
                  +'dVlvNzNHOWtFTjdLRTB1R1JPQ2YzS2pyamZLdEovbnFuTGMKeFlwUnRiUXA1dXJQ\n'
                  +'N2sxTnZCSGphVS9NM1d0YzFHY0I2bHJtbVdMdHExMFA4Z0JYazJxZHRFRXpTR0dC\n'
                  +'NWJTRAo4K1lkTGZ4TkdGdm9MbC9yQ29iQ3NzdnpaWGduT05SeG9MZC9TSXVEeE93\n'
                  +'S3JVSUNlSy9Wa3pPTTlEZkdiUnBECjh3S0JnUUNZeDZhVzhYcnR4UnB3L2dRWjdH\n'
                  +'MjlHUnBjek1UOHl2OENEMDg2dHlMZHRpbGs0Mk12Q1hGK2U3NFYKMVFDNklTRVg3\n'
                  +'TW5QZCtpQ2MxZ0VoQVFrOGFlOUYzM1BVdGV5TFd4aTdod1Bwd04zUHkrcXdwcmxU\n'
                  +'RjhzejNmTgpoczdXTG5CcG5wNzZzWjNCNW5GWWFwY09yZzF4YVlNWDRtUTdOOGRN\n'
                  +'SDNSVHhFakwxZz09Ci0tLS0tRU5EIFJTQSBQUklWQVRFIEtFWS0tLS0tCg=='
    },
    brigeData: {
      'address': '5E18B57CFDCBC504E0807B6BD9831E7C124198B6',
      'pubKey':    'LS0tLS1CRUdJTiBQVUJMSUMgS0VZLS0tLS0KTUlJQklqQU5CZ2txaGtpRzl3MEJB\n'
                  +'UUVGQUFPQ0FROEFNSUlCQ2dLQ0FRRUF6WTRiS2ZxczJVZzdwQ2llQWJ3YwpuQ2pi\n'
                  +'YW8zRktMSWN2RkNpVmltcDJPQ2VlTDZJZ09lb2l2WWkydTNUL0N6TUd5TmhoUXZk\n'
                  +'Z0dFVXo2RlZUbmZICmtzUmpDYVZVbjR1SlUrZFdJRy93Rm5XOG1oa1FXRk1qSGlr\n'
                  +'OGF4KzJ5K3FHNGdId1lvdFBhQnFFaGJMeGt4TWQKWEtORVk4bjVQKzI1Uy9SaXhz\n'
                  +'TDdiWmsvVkJiNzhwblJsZWtSekZ6MHlTcVNkTVhRdFZlSWN1QnNGWWxTU083bwpX\n'
                  +'QW01bnhOWDVZblhEZElKbGR2eFBjaUtUamZobWdwMFd4d0tFK2FTTk5IeW1EYUJU\n'
                  +'Q2RNQjBxRnZHRkl0U0JoCi9lUGZJRk5NQmR2V3dkVEhTTWI1RWkrS3h5QkoyRjA4\n'
                  +'eHUvREVuSjY2eEJTSmRmbGF3OU41bVdldXZtM1g3eWsKdlFJREFRQUIKLS0tLS1F\n'
                  +'TkQgUFVCTElDIEtFWS0tLS0tCg==',
      'privKey':   'LS0tLS1CRUdJTiBSU0EgUFJJVkFURSBLRVktLS0tLQpNSUlFb2dJQkFBS0NBUUVB\n'
                  +'elk0YktmcXMyVWc3cENpZUFid2NuQ2piYW8zRktMSWN2RkNpVmltcDJPQ2VlTDZJ\n'
                  +'CmdPZW9pdllpMnUzVC9Dek1HeU5oaFF2ZGdHRVV6NkZWVG5mSGtzUmpDYVZVbjR1\n'
                  +'SlUrZFdJRy93Rm5XOG1oa1EKV0ZNakhpazhheCsyeStxRzRnSHdZb3RQYUJxRWhi\n'
                  +'THhreE1kWEtORVk4bjVQKzI1Uy9SaXhzTDdiWmsvVkJiNwo4cG5SbGVrUnpGejB5\n'
                  +'U3FTZE1YUXRWZUljdUJzRllsU1NPN29XQW01bnhOWDVZblhEZElKbGR2eFBjaUtU\n'
                  +'amZoCm1ncDBXeHdLRSthU05OSHltRGFCVENkTUIwcUZ2R0ZJdFNCaC9lUGZJRk5N\n'
                  +'QmR2V3dkVEhTTWI1RWkrS3h5QkoKMkYwOHh1L0RFbko2NnhCU0pkZmxhdzlONW1X\n'
                  +'ZXV2bTNYN3lrdlFJREFRQUJBb0lCQUcvbGwrdjJSZm5TVklVOApoeWN5bS9CMlhW\n'
                  +'dFg2YWdKclpMbDRqazlYUFJ1OGxXUWd3YUpGVHJ3V25FTFhvYThiQytweHdDQ1FR\n'
                  +'WU4weUprCkZPZ3VWRG9WSmtTdDk2OFNhcU4rSDJoeEJ5Vnkya1NZdUVrY21OVm1a\n'
                  +'VGw3ZmlycmRKbTdySHc0a2ZWOTJmNWUKTnh0MUlGcTVsaFVzY2I3WFRqT0d5ZTZo\n'
                  +'WGZWK3FQQWFSMGErR29MYmtGZCtIRzJFTzlDcDRTUWRpNkV6N0Z4UgpYYmtYSWRE\n'
                  +'UFU3SVRYSURRVjlYV3p2UFFCNUVwZ1docU1IWlhvU01PZkhybTJTWnkzMnhRRU92\n'
                  +'MHhCNkcyRXJQCjR3RkpuL0JOQ01IVTJ1dFRDZWFqNTJaMDVqSEprTG1zdWp6Mjlq\n'
                  +'c2VrT1FCVGxTU2VmelM3YVQ5eU9MZGdDOHEKRDRZRWRSRUNnWUVBOEdENTlFMTdv\n'
                  +'ZEltVUNpejg2cHh1WHZpdVAyVXgrKy9weUppMHhNWWRtQlNWZHlvSGhUcQpYamlr\n'
                  +'bnNlbTJqSklFZXM3MEl3NTJhWDRZT3hKc21LT2lUTjdURE5sVFN2TFoySmFpMnNl\n'
                  +'dVpia0JldHRGdEJmCm9ITGlHMmVZWjlCYTgvZThONk80QWhZcVQzdE16aFBCUlpM\n'
                  +'dm5QT2M5WlQ2WjIxVVN0UEJJOHNDZ1lFQTJ1bksKWmlmb0VxYzNINGhuVlVBZ21U\n'
                  +'ZUlkdzNaaXpoY1NhUVhMd2Fka0d2MFVuRU5vdVQrN3NrVEYxR3NZaGdZTzJoLwpt\n'
                  +'dWpZVkVCdndWQUFuYldsZ1J1WnJIaXJtMW1ZWXE1YWtURjJDZXVvR0xTT3ZzQlZ6\n'
                  +'STRxektRb1hPbW9RUVhqCkZOemtkNXlQRDBqVWkxd3lZcWlTUlpqTnFlcFVmWUZv\n'
                  +'N1pDd21KY0NnWUI3QzNrSFNCNVFBQzJpZktOWXlGZTgKZGFEaHh0UkE0QVYvdlVk\n'
                  +'TytvaGNHQVF0ZFcwTk1QR2pWMlZVN2FnSUt4TGZNVVdHQkRHY3FxdVFtWU5ENzlV\n'
                  +'OApnQURPU2NUQ0V2ZmtyT0RpV3V2NFhqZk1tKzFVYjBkejd6cTBnbnZ1TUNaT21m\n'
                  +'SmJuTStWS2Rsd3BCSDFyd0VRCjV4MEdmMkZLQ0grQkwrS2xCdTNucndLQmdBRWN3\n'
                  +'UCtGNG1MOTRJVFRmNXJxU1lBZHpSNjF2azQ3eWhSQnRqQngKK3lqNXMzTWJ5Zkx5\n'
                  +'OE95cnJ1SW1jeGJJZ284Q3QrdEVsWnpQK3dGamM2MVpkKzdSQWhYalF4UDEvNVU0\n'
                  +'eDVNSApTazlBbUVrUDJjM1VVbHRJdWFUeWpKWDBSZnVXbVJISkVZd2VDb2dNV2JB\n'
                  +'ZmxzM3poMUs5TlFaUS8zN253bTI2Cmwzc2ZBb0dBQ2VndmYrUTI5Zi9WRWg5WkZT\n'
                  +'dS80YVYrOWorRDBWSmlzVjBGUG5nS0tkWEorVWgrdGhJTmtiOSsKT000RXF6Tktw\n'
                  +'T29yeXd3WXhFRkQ0c0YvWVpRM1V3RUZZMnNCNk9yYzFsTHd2VXR0UVFqZnMzK1lr\n'
                  +'OXJ4SHF3MgpLTXdWYUVkOE5wMDN6RUZBT3NydUJIc0NKWU9rZWZGTXBTQUFiMUFv\n'
                  +'YVAweFFVUFNoSEE9Ci0tLS0tRU5EIFJTQSBQUklWQVRFIEtFWS0tLS0tCg=='
    },
    timeOut: 5000, /*Time out de getCOntract>Function*/
    options: {
      callType: 2,
      fee: 0,
      gas: 1000,
      amount: 1,
      language: 'sol'
    },
    ipfs:{
      endpoint: 'http://192.168.99.100:5001/api/v0/'
    }
  },
  solidity:{
    url:'http://solidity:8081/smartPayment',
    contract:'smartPayment'
  },
  providers: {
    proxy:{
      needed:false,
      url:'http://proxy.eng.gsnetcloud.corp:8080/'
    },
    santanderSpain: {
      authServer: {
        baseUrl:'http://sanes:1080/annapurna/sb',
        services: {'get_token':'/get_token'},
        form: {
          client_id: '24cd2141-bb37-4d4d-9316-ff88da4987cb',
          client_secret: 'K3fI4eF7eT2eY3iO2rK7oJ8mT3uQ7wQ6eT8pP0oQ6dJ7fO4kV3',
          username: 'x021096',
          password: 'x021096',
          scope: 'payments.change payments.read accounts.read contracts.read',
          grant_type: 'password'
        }
      },
      baseUrl:'http://sanes:1080/annapurna/sb',
      clientID: '24cd2141-bb37-4d4d-9316-ff88da4987cb',
      testAccount: '004946333000501481',
      testBridgeAccount: '004900013000536503',
      minFare: 2.03
    },
    santanderUK: {
      authServer:{
        tokenTTL: 4*60*1000, //4m
        endpoint: 'http://sanuk:1080/SUKMOV_WSI_ENS/ws/SUKMOV_WSI_Def_Listener'
      },
      position:{
        endpoint: 'http://sanuk:1080/ABB_MBANWS_GblPos_ENS/ws/MBANWS_Def_Listener'
      },
      transfer:{
        endpoint: 'http://sanuk:1080/ABB_MBANWS_Payments_ENS/ws/MBANWS_Def_Listener',
      },
      euroChange: 0.85,
      testBridgeAccount: '0000000000000000000'
    },
    bbva: {}
  },
  sharedKeys: {
    key: 'genericKey'
  },
  publicKeyProvider: {// URL --> 'https://security-srv-keymanager-serenity-identity-pre.appls.boae.paas.gsnetcloud.corp:443/v1/publicKey'
    keyIdentifier: 'idpserenity_sha1withrsa',
    defaultKey: 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjAUCdrKSsrwkfzcqsiBhecKuTOriACZ0GkTh0OGY59bcYyox6nPCE2q9YBAEWtZkc9sDsy1BJq8PU748GOjSptUsWIYDy02Yzl8+rDsCp+wYSwixunE5sfL9wyHJE1qq79j7K3FcJiqsQUVdvZbB2oeDaS7s35JjiWCQUWkxMGuuuqGncXbFVfOOalpbL+QoT9Br9mbpsy6sQn17IVQA1M+YRzo1hEOauPGdPa1VwTcNz8cJ/CMzIJMK41Woi0q7VlguYTzO+cbLf+UQOW/L4D1TI2ipeLOMhuYgBsKOwXifiQnVDGVXEKoIaFqRvA71uGecdalnynaP4WoeTFvcZwIDAQAB',
    maxAttemps: 1,
    delayBetweenAttemps: 50,
    url: ''
    //url: 'http://sanes:1080/v1/publicKey'
  },
  erisFailOver: {
    url: '192.168.99.100',
    port: 1337,
    delayAttemps: 3000
  }
};
