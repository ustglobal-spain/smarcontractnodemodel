'use strict';

var appRoot = require('app-root-path');
var config = require(appRoot + '/configuration.js').config.eris.ipfs;
var Log = require('log'),
  log = new Log(require(appRoot + '/configuration.js').config.logLevel); // debug, info, error
var request = require('request');
var coding = require('./coding.js');

const servers = config.endpoint.servers;
const basePath = config.endpoint.basePath;

var IPFSServer = function (serverIndex) {
  var i = serverIndex % servers.length;
  return servers[i] + basePath;
}

function jsonizeResponse(addResponse) {
  addResponse = addResponse.replace(/(?:\r\n|\r|\n)/g, ',');
  addResponse = addResponse.slice(0, -1); //removing last comma
  addResponse = `[${addResponse}]`;
  log.debug(`[ipfs] f(jsonizeResponse) parsing: ${addResponse}`);
  return JSON.parse(addResponse);
};

var saveOnServer = (files, serverIndex) => {
  /* files is an array like :
  [  {
      "name": "test.txt",
      "payload": "abacd1"
    }, .....
  ]
  */
  log.debug(`[ipfs] f(save) in, preparing request `)
  var resultPromise = new Promise(function (resolve, reject) {
    var req = request.post(IPFSServer(serverIndex) + 'add?w', function (err, resp, body) {
      if (err) {
        log.error(`[ipfs] f(save) error saving: ${err} `);
        reject(err);
      } else {
        log.info(`[ipfs] f(save) saved (raw=${body})`);
        var jsonizedResponse = jsonizeResponse(body);
        log.info(`[ipfs] f(save) saved (processedData=${jsonizedResponse})`);
        resolve(jsonizedResponse);
      }
    });
    var form = req.form();

    files.forEach(file => {
      log.debug(`[ipfs] f(save) processing ${file.name}`);
      var binFile = coding.hex2raw(file.payload);
      log.info(`[ipfs] f(save) bin file size ${binFile.length} `);
      form.append('file', binFile, {
        filename: file.name
        //contentType: 'text/plain'
      });
    });
  });
  return resultPromise;
};

var pin = function (hash, serverIndex) {
  log.debug(`[ipfs] f(pin) in, preparing request `)
  var resultPromise = new Promise(function (resolve, reject) {
    var req = request.post(IPFSServer(serverIndex) + 'pin/add?arg=' + hash, function (err, resp, body) {
      if (err) {
        log.error(`[ipfs] f(pin) error pinning: ${err} `);
        reject(err);
      } else {
        var bodyjson = JSON.parse(body);
        if (bodyjson.Code === 0) {
          reject(bodyjson);
        }
        else {
          log.info(`[ipfs] f(pin) pinned ${JSON.stringify(body)} `);
          resolve(JSON.parse(body));
        }
      }
    });
  });
  return resultPromise;

};

var jsonBody = function (str) {
  var parse = str.split('{');
  if (parse.length > 1) str = '{' + parse[1];
  else str = parse[0];
  try {
    var bodyjson = JSON.parse(str);
  }
  catch (err) {
    var bodyjson = {
      data: str
    }
  }
  return bodyjson;
}

var get = function (hash, serverIndex) {
  log.debug(`[ipfs] f(get) in, preparing request `)
  var bodyjson = '';
  var resultPromise = new Promise(function (resolve, reject) {
    var req = request.post(IPFSServer(serverIndex) + 'cat?arg=' + hash, function (err, resp, body) {
      if (err) {
        log.error(`[ipfs] f(get) error getting file: ${err} `);
        reject(err);
      } else {
        try {
          bodyjson = jsonBody(body);
          if (bodyjson.Code === 0) {
            reject(bodyjson);
          }
          else {
            bodyjson = jsonBody(body);
            log.info(`[ipfs] f(get) got a file ${JSON.stringify(bodyjson)} `);
            resolve(bodyjson);
          }
        }
        catch (error) {
          var resp = {
            Message: 'It is a directory',
            Code: 0
          }
          reject(resp);
        }
      }
    });
  });
  return resultPromise;

};

var getFileLinks = function (hash, serverIndex) {
  log.debug(`[ipfs] f(getFileLinks) in, preparing request `)
  var bodyjson = '';
  var resultPromise = new Promise(function (resolve, reject) {
    var req = request.post(IPFSServer(serverIndex) + 'ls?arg=' + hash, function (err, resp, body) {
      if (err) {
        log.error(`[ipfs] f(getFileLinks) error getting file: ${err} `);
        reject(err);
      }
      else {
        var jsonbody = JSON.parse(body);
        if ((jsonbody.Objects) && (jsonbody.Objects.length > 0)) {
          log.info(`[ipfs] f(getLinks) got a file ${JSON.stringify(jsonbody.Objects[0].Links)} `);
          resolve(jsonbody.Objects[0].Links);
        }
        else {
          reject(body);
        }
      }
    });
  });
  return resultPromise;

};

var retryer = function (params, promisedFunction) {
  return new Promise((resolve, reject) => {
    promisedFunction(params, 0)
      .then(x => {
        resolve(x)
      })
      .catch(reason => {
        log.warning(`[ipfs] f(retryer) error, retrying  ${reason}`);
        promisedFunction(params, 1)
        .then(x => {
          resolve (x);
        })
        .catch(reason => {
          log.error(`[ipfs] f(retryer) error, retrying  ${reason}`);
          reject(reason);
        })
      })
  })
};

module.exports = {
  save: function (files) { return retryer(files, saveOnServer); },
  pin: function (hash) { return retryer(hash, pin); },
  get: function (hash) { return retryer(hash, get); },
  getFileLinks: function (hash) { return retryer(hash, getFileLinks); }
};
