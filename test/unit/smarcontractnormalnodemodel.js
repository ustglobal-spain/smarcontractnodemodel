'use strict';

var appRoot = require('app-root-path');
var config = require(appRoot + '/configuration.js');
var chai = require('chai');
chai.should();
var expect = chai.expect;
var lib = require('../../type/normal.js');
lib.setPlatform('eris');
var erisDbImpl = require('erisdb_model');
var eris = erisDbImpl.getImplementation('eris');
var sinon = require('sinon');
var uuid = require('uuid');
var crypto = require('../../crypto.js');

describe('smarcontractnodemodel module', function () {
  it('exports object', function () {
    expect(lib).to.be.an('object');
  });
});

describe('having a module', function () {

  describe('creating a correct contract', function () {
    this.timeout( 5000 );
    var correctContract = {
      payId:'654654654654',
      sourceAccount:'sac',
      sourceName:'javier mansilla perez',
      destinationAccount:'dac',
      destinationName:'dname garcia perez',
      destinationBridgeAddress:'12345678912332112345678912332155',
      subject:'creating a correct contract',
      amount:15.3,
      purchasing: 'normal'
    };

    var getterContract = {
      payId:'654654654654',
      sourceAccount:'sac',
      sourceName:'javier mansilla perez',
      destinationAccount:'dac',
      destinationName:'dname garcia perez',
      destinationBridgeAddress:'12345678912332112345678912332155',
      subject:'creating a correct contract',
      amount:15.3,
      purchasing: 'normal'
    }

    var contractInfo = {
    };

    var retrivedContract = {}; //to be filled with the gotContract

    var erisStub;
    var uidStub;

    before (function(done) {
      erisStub = sinon.stub(eris, 'callContractFunction', function(abi,address,verb,content,data){
        for ( var attrName in content ) { contractInfo[attrName] = content[attrName]; } //store all properties in contractInfo
        return new Promise((resolve, reject) => {

          resolve(contractInfo);
        })
      });

      uidStub = sinon.stub(uuid, 'v1').returns('3b7cb2f0c5fb11e69934e74cb64db10e');
      done();
    });

    after (function(done) {
      erisStub.restore();
      uidStub.restore();
      done();
    });

    it('a creator should create the contract and respond',function (done) {
        lib.functions.send(correctContract)
        .then( x => {
          sinon.assert.calledTwice(erisStub);
          done();
        });
        //first event, sent_ok
        lib.streams.sentStream.onNext({type:'eventSent',args:{payId:'654654654654'
        ,from:'080B6B5AD38EC10E96A42DA1131FE29EB3ECC9C8'
        ,purchasing: 'mihash'}})

      });

    it('a creator should be able to retrive the payment', function (done){
      lib.functions.get(correctContract.payId,'source') //source, bridge, auditor
      .then( x => {
        retrivedContract = x;
        //expect(x).to.have.all.properties.of(correctContract);
        done();
      });
    });

    it('the contract got by the creator should contain the original data', done => {
      expect(retrivedContract).to.be.ok;
      retrivedContract.should.not.be.null;
      retrivedContract.sourceName.should.not.be.equal(contractInfo.sourceName);
      getterContract.purchasing = retrivedContract.purchasing;
      // getterContract.keys = retrivedContract.keys;
      retrivedContract.should.include(getterContract);
      done();
    });

    it('a bridge should be able to retrive the payment', function (done){
      retrivedContract = {};
      lib.functions.get(correctContract.payId,'bridge') //source, bridge, auditor
      .then( x => {
        retrivedContract = x;
        //expect(x).to.have.all.properties.of(correctContract);
        done();
      });
    });

    it('the contract got by the bridge should contain the original data', done => {
      expect(retrivedContract).to.be.ok;
      retrivedContract.should.not.be.null;
      retrivedContract.sourceName.should.not.be.equal(contractInfo.sourceName);
      getterContract.purchasing = retrivedContract.purchasing;
      // getterContract.keys = retrivedContract.keys;
      retrivedContract.should.include(getterContract);
      done();
    });

    it('a auditor should be able to retrive the payment', function (done){
      retrivedContract = {};
      lib.functions.get(correctContract.payId,'auditor') //source, bridge, auditor
      .then( x => {
        retrivedContract = x;
        //expect(x).to.have.all.properties.of(correctContract);
        done();
      });
    });

    it('the contract got by the auditor should contain the original data', done => {
      expect(retrivedContract).to.be.ok;
      retrivedContract.should.not.be.null;
      retrivedContract.sourceName.should.not.be.equal(contractInfo.sourceName);
      getterContract.purchasing = retrivedContract.purchasing;
      // getterContract.keys = retrivedContract.keys;
      retrivedContract.should.include(getterContract);
      done();
    });

    it(' anyother should be able to retrive the payment', function (done){
      retrivedContract = {};
      lib.functions.get(correctContract.payId,'nonone') //source, bridge, auditor
      .then( x => {
        retrivedContract = x;
        //expect(x).to.have.all.properties.of(correctContract);
        done();
      });
    });

    it('the contract got by the anyother should be chiphered', done => {
      expect(retrivedContract).to.be.ok;
      retrivedContract.should.not.be.null;
      retrivedContract.payId.should.be.equal(correctContract.payId);
      retrivedContract.destinationBridgeAddress.should.be.equal(correctContract.destinationBridgeAddress);
      done();
    });

  });
});
