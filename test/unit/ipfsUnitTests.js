'use strict';

const chai = require('chai');
chai.should();
const expect = chai.expect;
const lib = require('../../ipfs.js');
const request = require('request');
const sinon = require('sinon');
const formData = require('form-data');

describe('testing pin function success', function () {
  this.timeout(8000);

  let ipfsMock;
  const miHash = 'QmarNgpyJHwcrLfjPzFsLiPmLukU3uYqqofHoYHbnUfErn';
  before(function (done) {
    const expected = {
      Pins: [
        "QmarNgpyJHwcrLfjPzFsLiPmLukU3uYqqofHoYHbnUfErn"
      ]
    }
    ipfsMock = sinon.stub(request, 'post').yields(null, null, JSON.stringify(expected));
    done();
  });
  after(function (done) {
    ipfsMock.restore();
    done();
  });

  it('you can pinned a file', function (done) {
    lib.pin(miHash)
      .then(response => {
        try {
          sinon.assert.calledOnce(ipfsMock);
          done();
        }
        catch (err) {
          done(err);
        }
      })
  })
})

describe('testing pin function error response', function () {
  this.timeout(5000);
  let ipfsMock;
  const miHash = '';
  before(function (done) {
    const expected = {
      Message: "pin: invalid ipfs ref path",
      Code: 0
    }
    ipfsMock = sinon.stub(request, 'post').yields(null, null, JSON.stringify(expected));
    done();
  });
  after(function (done) {
    ipfsMock.restore();
    done();
  });

  it('you can not pinned a file with no hash', function (done) {
    lib.pin(miHash)
      .then(response => {
        console.log(response);
      })
      .catch(err => {
        try {
          sinon.assert.calledTwice(ipfsMock);
          done();
        }
        catch (error) {
          done(error);
        }
      })
  })
})

describe('testing pin function error server', function () {
  this.timeout(5000);
  let ipfsMock;
  const miHash = '';
  before(function (done) {
    ipfsMock = sinon.stub(request, 'post').yields('You have been rapped for a black man', null, null);
    done();
  });
  after(function (done) {
    ipfsMock.restore();
    done();
  });

  it('you can not pinned a file when an error happend', function (done) {
    lib.pin(miHash)
      .then(response => {

      })
      .catch(err => {
        try {
          sinon.assert.calledTwice(ipfsMock);
          done();
        }
        catch (error) {
          done(error);
        }
      })
  })
})

describe('testing save function success', function () {
  this.timeout(5000);
  let ipfsMock;
  before(function (done) {
    const expected = [{ Name: "TestUnit", Hash: "QmarNgpyJHwcrLfjPzFsLiPmLukU3uYqqofHoYHbnUfErn" }
      , { Name: "", Hash: "QmXNvqSmX2s8G5EGgWWYRy1PsRhwHoxQqu9nb3fHqUSuXj" }];
    const str = JSON.stringify(expected);
    const str2 = str.substr(1);
    ipfsMock = sinon.stub(request, 'post').yields(null, null, str2);

    done();
  });
  after(function (done) {
    ipfsMock.restore();
    done();
  });

  it('you can save a file', function (done) {
    const file = [{
      name: 'TestUnit',
      payload: 'FF'
    }]

    lib.save(file)
      .then(response => {
        try {
          sinon.assert.calledOnce(ipfsMock);
          done();
        }
        catch (err) {
          done(err);
        }
      })

  })
})

describe('testing save function error server', function () {
  this.timeout(5000);
  let ipfsMock;
  before(function (done) {
    ipfsMock = sinon.stub(request, 'post').yields('Forced ass!!', null, null);

    done();
  });
  after(function (done) {
    ipfsMock.restore();
    done();
  });

  it('you can not save a file when an error happend', function (done) {
    const file = [{
      name: 'TestUnit',
      payload: 'FF'
    }]

    lib.save(file)
      .then(response => {

      })
      .catch(error => {
        sinon.assert.calledTwice(ipfsMock);
        done();
      })

  })
})

describe('testing get function success', function () {
  this.timeout(5000);
  let ipfsMock;
  before(function (done) {
    const expected = { Data: "faileContent" };
    const str = JSON.stringify(expected);
    ipfsMock = sinon.stub(request, 'post').yields(null, null, str);

    done();
  });
  after(function (done) {
    ipfsMock.restore();
    done();
  });

  it('you can get a file', function (done) {
    var hashFile = 'QmXNvqSmX2s8G5EGgWWYRy1PsRhwHoxQqu9nb3fHqUSuXj'

    lib.get(hashFile)
      .then(response => {
        try {
          sinon.assert.calledOnce(ipfsMock);
          done();
        }
        catch (err) {
          done(err);
        }
      })

  })
})

describe('testing get function error response', function () {
  this.timeout(5000);
  let ipfsMock;
  const miHash = '';
  before(function (done) {
    const expected = {
      Message: " invalid ipfs ref path",
      Code: 0
    }
    ipfsMock = sinon.stub(request, 'post').yields(null, null, JSON.stringify(expected));
    done();
  });
  after(function (done) {
    ipfsMock.restore();
    done();
  });

  it('you can not got a file with no hash', function (done) {
    lib.get(miHash)
      .then(response => {

      })
      .catch(err => {
        try {
          sinon.assert.calledTwice(ipfsMock);
          done();
        }
        catch (error) {
          done(error);
        }
      })
  })
})

describe('testing get function error server', function () {
  this.timeout(5000);
  let ipfsMock;
  const miHash = '';
  before(function (done) {
    ipfsMock = sinon.stub(request, 'post').yields('Fatal error', null, null);
    done();
  });
  after(function (done) {
    ipfsMock.restore();
    done();
  });

  it('you can not got a file when an error happend', function (done) {
    lib.get(miHash)
      .then(response => {

      })
      .catch(err => {
        try {
          sinon.assert.calledTwice(ipfsMock);
          done();
        }
        catch (error) {
          done(error);
        }
      })
  })
})
