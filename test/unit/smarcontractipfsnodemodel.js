'use strict';

var appRoot = require('app-root-path');
var config = require(appRoot + '/configuration.js');
var chai = require('chai');
chai.should();
var expect = chai.expect;
var lib = require('../../type/ipfs.js');
lib.setPlatform('eris');
var erisDbImpl = require('erisdb_model');
var eris = erisDbImpl.getImplementation('eris');
var sinon = require('sinon');
var ipfs = require('../../ipfs.js');
var uuid = require('uuid');
var crypto = require('../../crypto.js');

describe('smarcontractnodemodel module', function () {
  it('exports object', function () {
    expect(lib).to.be.an('object');
  });
});

describe('having a module', function () {

  describe('creating a correct contract', function () {
    this.timeout( 5000 );
    var correctContract = {
      payId:'654654654654',
      sourceAccount:'sac',
      sourceName:'javier mansilla perez',
      destinationAccount:'dac',
      destinationName:'dname garcia perez',
      destinationBridgeAddress:'12345678912332112345678912332155',
      subject:'creating a correct contract',
      amount:15.3,
      attachments: [{
        name: 'test',
        payload: 'AF'
      }]
    };

    var getterContract = {
      payId:'654654654654',
      sourceAccount:'sac',
      sourceName:'javier mansilla perez',
      destinationAccount:'dac',
      destinationName:'dname garcia perez',
      destinationBridgeAddress:'12345678912332112345678912332155',
      subject:'creating a correct contract',
      amount:15.3,
      purchasing: 'Qmcwf3fHp5Gd4Adpng2bBwHFe8jkzSticnWJzKt6Be6ULJ'
    }

    var expectedGet = {
      encriptedUUIDOrigin: 'CN+uPHm9KovIFEl7Ac2fYvkUh5WW7g7BKFC1vTLhKthhGTAWtSi6DfO4n7JegJnxHQoWcKfHU76cfvTj7MaXWFTPZyJC3EUN2rvdgBbU+Rwkj1q8cjJ7RSSOsl/N/qqzsZF8vl2uoiPwIh0B2mWmMZQxAy3gXXpycWki5hIjFiMFHCd9S7uV9xxS914TR3aknERUImm0ajjkTqfEA40O9X/EtqYKGDSVsWTHuUBGyY+USoVEqU41yNuuAB220vMLpHdVxvISZ/sNYpi3WnPG8jzOcgIvlb/3GcG3OEFpSDbyKFNPVP01AJC28xhj7ozoUz3HrFZU95yJ/yFPQXG2Zg==',
      encriptedUUIDDestiny: 'u15YiNMJDiZW1TgWAizsL70RBhCdyvgiwngGaPqq0GbbtE9XhYR3pxiQFp6s99mQIvdT3HVNpzqe+c0cSKGYhsVBH+Vios4jGJRiUOHDN5Rcu1UGNj8Rk6nZRYxZWkzwok3EM2JrRfji20qU4zlhSJGA2z4fZPcVToOf2n8enaYvUqlKUYVrtBFWAkeCd7j8ZxJWtPt4dFNOt4ngMSmau0TI195NUDgKRWKkQQyLycF90UkF+leiDfHXQnmSaGL9nUk/sTAskrNfwxkKjq/eyezmu/Y5jtuKzNRqtvK9D7hfPKtXPIYKb1CbqAbDVJocGTQnh6TbB4lqd9HTOtYbHg==',
      encriptedUUIDAuditor: 'wxu5Mll5BCu6Asdaj02fz7qO4AGYNX0pc5lnlAfIOJBXlJh6/UTFEBRBc1KhxyLZWKaWss5CqzSV5E+0ge2EAQqRJ2JD14eV8ZRG5zciL7i3SNYdylQLoDXHSUfq82JbsjU5i/QyP0Ah8r0w+ZbjGJY32ULt2Q0MLeYXmk8HoMx4OQ9Ae8mTeC2Aed+gA17eUakJtIXtUavN5E+aTtIWXU74Xyb6OIT+8sl+6leVKKVp5/TKS0N2wN2KNqe8BFL6iZ0n7B+Oi61cCZ6iaapiSEoNc7ud0EIV2B0tZ0h+trCfnkfskd5vr1r56tqPme1L+Qd4FMiiEMcP13fBSQtjXg=='
    }

    var expectedData = "U2FsdGVkX1/A+CeTLlicmI5ayNopsawqLAzDDI7gyKUmAruSkEh4zZfX9ZgCSvIn7WFAIqxsjZTCiJfU+dBXNdCI/LjTffHxBUleahTa7WwW1Zg8Vo7k2lcXSdaolwoc3NuE5k8hZABzz1YwLYZAEShtzU0DI9Koij3/WFN0MVZYFeK63Nfzz5M+uhbtKf2DYIm+Kciy+9nFoskB9hkA4Qi8PFktgRatp+eol0GvJj1Fpts66nhYIoQsMo+lfYViMxo8h5zMTMVbhh+2g9oh9A=="

    var expectedLinks = [{
      Name: 'mock',
      Hash: 'QmYpxWTEGXK3XvwmnLuGvLkxpSjeqsZHDmveLwnK3RyviK',
      Size: 52,
      Type: 2
    }]

    var expectedSave = [{
      Name: 'mock',
      Hash: 'Qmcwf3fHp5Gd4Adpng2bBwHFe8jkzSticnWJzKt6Be6ULJ'
    },
    {
      Name: 'mock',
      Hash: 'Qmcwf3fHp5Gd4Adpng2bBwHFe8jkzSticnWJzKt6Be6ULJ'
    }]

    var contractInfo = {
    };

    var retrivedContract = {}; //to be filled with the gotContract

    var erisStub;
    var ipfsMock;
    var ipfsSaveMock;
    var ipfsGetLinksMock;
    var uidStub;

    before (function(done) {
      erisStub = sinon.stub(eris, 'callContractFunction', function(abi,address,verb,content,data){
        for ( var attrName in content ) { contractInfo[attrName] = content[attrName]; } //store all properties in contractInfo
        return new Promise((resolve, reject) => {

          resolve(contractInfo);
        })
      });

      ipfsSaveMock = sinon.stub(ipfs, 'save').returns( new Promise((resolve, reject) => {resolve(expectedSave);}));
      ipfsGetLinksMock = sinon.stub(ipfs, 'getFileLinks').returns( new Promise((resolve, reject) => {resolve(expectedLinks);}));
      uidStub = sinon.stub(uuid, 'v1').returns('3b7cb2f0c5fb11e69934e74cb64db10e');
      done();
    });

    after (function(done) {
      erisStub.restore();
      ipfsSaveMock.restore();
      ipfsGetLinksMock.restore();
      uidStub.restore();
      done();
    });

    beforeEach (function(done) {
      ipfsMock = sinon.stub(ipfs, 'get');
      ipfsMock.onFirstCall().returns( new Promise((resolve, reject) => {resolve(expectedGet);}));
      ipfsMock.onSecondCall().returns( new Promise((resolve, reject) => {resolve(expectedData);}));
      done();
    });

    afterEach (function(done) {
      ipfsMock.restore();
      done();
    });

    it('a creator should create the contract and respond',function (done) {
        lib.functions.send(correctContract)
        .then( x => {
          sinon.assert.calledOnce(erisStub);
          done();
        });
        //first event, sent_ok
        lib.streams.sentStream.onNext({type:'eventSent',args:{payId:'654654654654'
        ,from:'080B6B5AD38EC10E96A42DA1131FE29EB3ECC9C8'
        ,purchasing: 'mihash'}})

      });

    it('a creator should be able to retrive the payment', function (done){
      lib.functions.get(correctContract.payId,'source') //source, bridge, auditor
      .then( x => {
        retrivedContract = x;
        //expect(x).to.have.all.properties.of(correctContract);
        done();
      });
    });

    it('the contract got by the creator should contain the original data', done => {
      expect(retrivedContract).to.be.ok;
      retrivedContract.should.not.be.null;
      retrivedContract.sourceName.should.not.be.equal(contractInfo.sourceName);
      getterContract.purchasing = retrivedContract.purchasing;
      // getterContract.keys = retrivedContract.keys;
      retrivedContract.should.include(getterContract);
      done();
    });

    it('a bridge should be able to retrive the payment', function (done){
      retrivedContract = {};
      lib.functions.get(correctContract.payId,'bridge') //source, bridge, auditor
      .then( x => {
        retrivedContract = x;
        //expect(x).to.have.all.properties.of(correctContract);
        done();
      });
    });

    it('the contract got by the bridge should contain the original data', done => {
      expect(retrivedContract).to.be.ok;
      retrivedContract.should.not.be.null;
      retrivedContract.sourceName.should.not.be.equal(contractInfo.sourceName);
      getterContract.purchasing = retrivedContract.purchasing;
      // getterContract.keys = retrivedContract.keys;
      retrivedContract.should.include(getterContract);
      done();
    });

    it('a auditor should be able to retrive the payment', function (done){
      retrivedContract = {};
      lib.functions.get(correctContract.payId,'auditor') //source, bridge, auditor
      .then( x => {
        retrivedContract = x;
        //expect(x).to.have.all.properties.of(correctContract);
        done();
      });
    });

    it('the contract got by the auditor should contain the original data', done => {
      expect(retrivedContract).to.be.ok;
      retrivedContract.should.not.be.null;
      retrivedContract.sourceName.should.not.be.equal(contractInfo.sourceName);
      getterContract.purchasing = retrivedContract.purchasing;
      // getterContract.keys = retrivedContract.keys;
      retrivedContract.should.include(getterContract);
      done();
    });

    it(' anyother should be able to retrive the payment', function (done){
      retrivedContract = {};
      lib.functions.get(correctContract.payId,'nonone') //source, bridge, auditor
      .then( x => {
        retrivedContract = x;
        //expect(x).to.have.all.properties.of(correctContract);
        done();
      });
    });

    it('the contract got by the anyother should be chiphered', done => {
      expect(retrivedContract).to.be.ok;
      retrivedContract.should.not.be.null;
      retrivedContract.payId.should.be.equal(correctContract.payId);
      retrivedContract.destinationBridgeAddress.should.be.equal(correctContract.destinationBridgeAddress);
      retrivedContract.should.have.property('data');
      done();
    });

  });
});
