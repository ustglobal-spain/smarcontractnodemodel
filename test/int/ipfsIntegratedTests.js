'use strict';

var chai = require('chai');
chai.should();
var expect = chai.expect;
var lib = require('../../ipfs.js');
var coding = require('../../coding.js');

describe('ipfs module', function () {
  it('exports object', function () {
    expect(lib).to.be.an('object');
  });
});

describe('having a ipfs module', function(){
  this.timeout( 8000 );
  var miHash = '';
  var hashKeys = '';

  it('storing a file into ipfs server', function(done){
    var file = [{
      name: 'file01',
      payload: 'FF'
    }]
    lib.save(file)
    .then(response =>{
      try{
        response.should.be.instanceof(Array).and.have.lengthOf(2);
        response.should.to.have.deep.property('[0].Name', 'file01');
        response.should.to.have.deep.property('[0].Hash');
        miHash = response[0].Hash;
        done();
      }
      catch(err){
        done(err);
      }
    })
  })
  it('storing some files into ipfs server', function(done){
    var file = [{
      name: 'file01',
      payload: 'FF'
    },
    {
      name: 'file02',
      payload: 'AA'
    }
  ]
    lib.save(file)
    .then(response =>{
      try{
        response.should.be.instanceof(Array).and.have.lengthOf(3);
        response.should.to.have.deep.property('[1].Name', 'file02');
        response.should.to.have.deep.property('[1].Hash');
        done();
      }
      catch(err){
        done(err);
      }
    })
  })
  it('storing 0 files into ipfs server', function(done){
    var file = [];
    lib.save(file)
    .then(response =>{
      try{
        response.should.be.instanceof(Array).and.have.lengthOf(0);
        done();
      }
      catch(err){
        done(err);
      }
    })
  })
  it('storing a file without directory into ipfs server', function(done){
    var setKeysData = {
      encriptedUUIDOrigin: 'Yd+aWtIWq8Qwh5FTnZs9fJm8NHJnOOsUgwyqHABEl4qzmOdi5ZBrOQfjFTvykKyPQU3ARtXkrhLzz35IeHDg4XteRpRObZADYugEEKaUwWltNEmfj7gLkx5acsvZw3pdnZF04nS+OW6a9Lz/KgyNnNK1CkNalVBp9Z1+WMDHytvoGYaAJCPPQrGzeP3dXF+E0qjPfqadENNIA+GiHnviI/XI7aLnFHNU0yC82foY8qGVhxYYCf2gUZMwDF1IuMu/vhQV3dzaAhfyEfpjrBAJW2QhF8xnkWSpj3T/Tbwx/m2kwzCpAIH7T0Ftr99qtD1/kFozNIsRZ0qFtW3NOB79Ag==',
      encriptedUUIDDestiny: 'l01ZbRVsUMI8M9tx4dU2+pWmzv35il2uyl3GgYUrDdivWnDnmwtBivYUqdaDfZniWuBTJCkg56dduxqPvqHLqrK1j+31RGNgw9QE5tRsMZxrtkCfIXplDzfJyiAS+zDUgCEM/8VB4R31V006ZUN8AQQowU0PKrd3ccUTfwkLqtcc8+ySV4aGz7SCnzBESg6kOpN0L7jZBAn7xJRZnrz2WUVLSP3aksp1RdTUscRQOPmq3l2Khgv5mYUrFb0IRy2/3fUKTHLDPSLAY0L3RWAJp/EX6MsE2QY3Mdrr3QTB1W19sDr/0FLKMz+b6wBh8tN9mZeqJSqGTiFU/cyFv440dA==',
      encriptedUUIDAuditor: 'asHlVzJBnFc6cTsst7ra+fc2wz+cZyUejOoBeuB9C+FhPXi5IQwPm57NkxXwjKGKOMpM7SP7XkW0trOYBrUSxWzcKh85P5ZXKBqiilK8bKxlVai8kHFy05qf+J4wf2gse0SvxlNQ2E0Kmaf6KyF8FNMBq1RCwKc4YYt8DzHjIKhrxyH2HacPR/wTIrNieIY9pE8XaxpVLVE86Vp+sm5uLuuvjKa5NBj5K6nivq1896TxJm1AzYqDHwEyJuYxJLMSeUqF96yrwEh2KdVYR5az+eCcr2Jh8iLq0NgSePSN9Kb0bhBHABmIknPdz6uW4d5InbIE9CHoXir9QFmK877wvQ=='
    };
    var files = [];
    var file = {
      name: 'encriptedKeys',
      payload: coding.convertToHex(JSON.stringify(setKeysData))
    }
    files.push(file);
    lib.save(files)
    .then(response =>{
      try{
        response.should.be.instanceof(Array).and.have.lengthOf(2);
        response.should.to.have.deep.property('[0].Name', 'encriptedKeys');
        response.should.to.have.deep.property('[0].Hash');
        hashKeys = response[0].Hash;
        done();
      }
      catch(err){
        done(err);
      }
    })
  })
  it('pin a file into ipfs server', function(done){
    lib.pin(miHash)
    .then(response =>{
      try{
        response.should.have.property('Pins');
        done();
      }
      catch(err){
        done(err);
      }
    })
  })
  it('can not pin a file into ipfs server with no hash', function(done){
    lib.pin(' ')
    .then(response =>{

    })
    .catch(err =>{
      try{
        err.should.have.property('Code');
        done();
      }
      catch(error){
        done(error);
      }
    })
  })
  it('can not get a file  with no hash', function(done){
    lib.get(' ')
    .then(response =>{

    })
    .catch(err =>{
      try{
        err.should.have.property('Code');
        done();
      }
      catch(error){
        done(error);
      }
    })
  })
  it('can get a file', function(done){
    lib.get(hashKeys)
    .then(response =>{
      response.should.have.property('encriptedUUIDOrigin').equals('Yd+aWtIWq8Qwh5FTnZs9fJm8NHJnOOsUgwyqHABEl4qzmOdi5ZBrOQfjFTvykKyPQU3ARtXkrhLzz35IeHDg4XteRpRObZADYugEEKaUwWltNEmfj7gLkx5acsvZw3pdnZF04nS+OW6a9Lz/KgyNnNK1CkNalVBp9Z1+WMDHytvoGYaAJCPPQrGzeP3dXF+E0qjPfqadENNIA+GiHnviI/XI7aLnFHNU0yC82foY8qGVhxYYCf2gUZMwDF1IuMu/vhQV3dzaAhfyEfpjrBAJW2QhF8xnkWSpj3T/Tbwx/m2kwzCpAIH7T0Ftr99qtD1/kFozNIsRZ0qFtW3NOB79Ag==');
      done();
    })
    .catch(err =>{
      done(err);
    })
  })
})
