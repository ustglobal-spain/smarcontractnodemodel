'use strict';

var appRoot = require('app-root-path');
var config = require(appRoot + '/configuration.js');
var crypto = require('crypto-js'),
    algorithm = 'aes-256-gcm';
var CryptoJS = require("crypto-js");
var Log = require('log'),
  log = new Log(config.logLevel);
var request = require('request');
var ursa = require('ursa');
var uuid = require('uuid');
var serverKeys = require('./providerServer.js');



function generateUUID(){
  return uuid.v1().replace('-','').replace('-','').replace('-','').replace('-','');
}

function encryptKey(pubPem, key){
  var pub, encryptedKey;
  //create public key with the pem public key
  pub = ursa.createPublicKey(pubPem, 'base64');
  //encrypt the uid with the publickey
  encryptedKey = pub.encrypt(key, 'utf8', 'base64');
  return encryptedKey;
}

function decryptKey(privPem, data){
  var priv, unenc;
  //create private key with the pem private key
  priv = ursa.createPrivateKey(privPem, '', 'base64')
  //decrypt the simetricKey with the privatekey
  unenc = priv.decrypt(data, 'base64', 'utf8');
  return unenc;
}

function cipher(text, pass){
  if (typeof(text) != 'undefined' && text != null){
    return CryptoJS.AES.encrypt(text, pass).toString();
  }
  else{
    return '';
  }
}

function decipher(text, pass){
  var bytes  = CryptoJS.AES.decrypt(text, pass);
  return bytes.toString(CryptoJS.enc.Utf8);
}


function cipherSimetricKeys(passphrase){
  var result = {
    encriptedUUIDOrigin: encryptKey(serverKeys.functions.getKeys('source').pubKey, passphrase),
    encriptedUUIDDestiny: encryptKey(serverKeys.functions.getKeys('bridge').pubKey,passphrase),
    encriptedUUIDAuditor: encryptKey(serverKeys.functions.getKeys('auditor').pubKey, passphrase)
  }
  return result;
}



module.exports = {
  generateUUID:generateUUID,
  encryptKey:encryptKey,
  decryptKey:decryptKey,
  cipher:cipher,
  decipher:decipher,
  cipherSimetricKeys:cipherSimetricKeys
}
