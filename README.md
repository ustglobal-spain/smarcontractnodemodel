# description

node module to handle an specific solidity contract called *smartPayment*

- It is shared by *fund servers* and *fund receivers*
- It uses reactive streams to handle contract events and eris library responses, in order
 to mix them and produce a simple response per operation to suit into an standard api
 interface

## dependencies

- a solidity server is needed
- an eris node is needed
- a configuration file needs to be in main application direcctory: `configuration.json`

## to run

to run tests:
```bash
mocha
```

to debug tests:
```bash
node-inspector --web-host=0.0.0.0 &
mocha --debug-brk
```

*the first process can be reused between debugs*
